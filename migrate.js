var path = require('path');

const config = require('./config.json')

require('sql-migrations').run({
        migrationsDir:path.resolve(__dirname,'src/sql'),
        host:config.database.host,
        port:config.database.port,
        db:config.database.database,
        user:config.database.user,
        password:config.database.password,
        adapter:'mysql'
});