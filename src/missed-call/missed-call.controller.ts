import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MissedCallService } from './missed-call.service';
import { CreateMissedCallDto } from './dto/create-missed-call.dto';
import { UpdateMissedCallDto } from './dto/update-missed-call.dto';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('missed-call')
@Controller('missed-call')
export class MissedCallController {
  constructor(private readonly missedCallService: MissedCallService) {}

  @Post('/create')
  create(@Body() createMissedCallDto: CreateMissedCallDto) {
    return this.missedCallService.create(createMissedCallDto);
  }

  @Get('/all')
  findAll() {
    return this.missedCallService.findAll();
  }

  @Get(':phone/:time')
  findOne(@Param('phone') phone: string, @Param('time') time: number) {
    return this.missedCallService.findOne(phone,time);
  }

  @Patch('/update/:id')
  update(@Param('id') id: string, @Body() updateMissedCallDto: UpdateMissedCallDto) {
    return this.missedCallService.update(+id, updateMissedCallDto);
  }

  @Delete('/delete/:id')
  remove(@Param('id') id: string) {
    return this.missedCallService.remove(+id);
  }
}