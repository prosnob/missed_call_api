CREATE TABLE `missed_call` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `phone` varchar(20) NOT NULL,
    `missed_timestamp` bigint(15) NOT NULL,
    `status` varchar(255) default 'PENDING',
    `time` datetime default now(),
    PRIMARY KEY(`id`)
)