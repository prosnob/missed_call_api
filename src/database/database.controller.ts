import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import * as config  from '../../config.json';
import { DatabaseService } from './database.service';
import { CreateDatabaseDto } from './dto/create-database.dto';
import { UpdateDatabaseDto } from './dto/update-database.dto';

@Controller('database')
export class DatabaseController {
  constructor(private readonly databaseService: DatabaseService) {
    if(config.autoUpdateDb){
      console.log("===Update DB ===");
      this.databaseService.updateDatabase();
    }

   
  }

  @Patch("update")
  updateDatabase() {
    this.databaseService.updateDatabase();
  }
}
