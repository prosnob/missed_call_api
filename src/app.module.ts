import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { MissedCallModule } from './missed-call/missed-call.module';

@Module({
  imports: [DatabaseModule, MissedCallModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
