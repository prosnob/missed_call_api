import { Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';
import { CreateMissedCallDto } from './dto/create-missed-call.dto';
import { UpdateMissedCallDto } from './dto/update-missed-call.dto';

@Injectable()
export class MissedCallService {
  private readonly table = 'missed_call';
  constructor( private readonly dbService:DatabaseService){}

  create(createMissedCallDto: CreateMissedCallDto) {
    return this.dbService.insert(this.table,this.dbService.parseColumnsDto(createMissedCallDto));
  }

  findAll() {
    return this.dbService.findAll(this.table);
  }

  findOne(phone: string,time:number) {
    return this.dbService.find(this.table,`phone = ${phone} AND status = 'PENDING' AND missed_timestamp BETWEEN ${time-60000} AND ${time}`);
  }

  update(id: number, updateMissedCallDto: UpdateMissedCallDto) {
    console.log(id,updateMissedCallDto);
    return this.dbService.update(this.table,this.dbService.parseColumnsDto(updateMissedCallDto),`id =${id}`);
  }

  remove(id: number) {
    return this.dbService.delete(this.table,`id=${id}`);
  }
}
