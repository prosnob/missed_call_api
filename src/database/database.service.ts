import { Injectable, InternalServerErrorException } from '@nestjs/common';
import  * as mysql from 'mysql';
import * as config from '../../config.json';
import * as SqlString from 'sqlstring';
// import { CreateDatabaseDto } from './dto/create-database.dto';
import { ColumnPair, ColumnsDto } from './dto/insert.dto';
// import { UpdateDatabaseDto } from './dto/update-database.dto';
import { timeStamp } from 'console';
import { CustomSQLDto } from './dto/custom-sql.dto';

@Injectable()
export class DatabaseService {
  
  con:any;
  sqlMode = 'TRADITIONAL';
  migrateTable = 'db_migration';
  constructor(){}
  private async createConnection(){
    if(typeof this.con === 'undefined' || this.con.state === 'disconnected'){
      this.con = mysql.createConnection({
        host:config.database.host,
        port:config.database.port,
        user:config.database.user,
        password:config.database.password,
        database:config.database.database,
        multipleStatements:true
      });
      await new Promise((resolve, reject) => {
        this.con.connect((err) => {
          if (err) {
            // // console.log("🚀 ~ file: database.service.ts ~ line 35 ~ DatabaseService ~ this.con.connect ~ err", err)
            reject(err)
          }
          else {
            resolve(this.con)
            console.log("Connected to DB");
          }
        });
      });
    }
  }

  private async closeConnection() {
    await new Promise((resolve, reject) => {
      this.con.end((err) => {
        if (err) {
          reject(err)
        }
        else {
          resolve(this.con)
          // console.log("The connection is terminated now")
        }
      });
    })
  }

  async executeSQL(sql:string,escapingValues?:any[]): Promise<any>{
    await this.createConnection();
    const queryPromise:Promise<any> = new Promise((resolve,reject) =>{
      const callback = (error,results,fields) =>{
        if(error){
          reject(error)
        }
        else{
          resolve(results)
        }
      };
      if (escapingValues !== undefined && escapingValues.length > 0){
        this.con.query(sql,escapingValues,callback);
      }else {
        this.con.query(sql,callback);
      }
    });
    return queryPromise;
  }

  customSQL(statment: CustomSQLDto): Promise<any> {
    const sql = statment.sql.trim();
    if (!sql) {
      return Promise.reject("sql undefined");
    }
    return this.executeSQL(sql);
  }

  findAll(tableName){
    const sql = `SELECT * From ${tableName};`;
    // const a = "SELECT * FROM `missed-call-verification`.call;";
    console.log(sql);
    return this.executeSQL(sql);
  }

  find(tableName,condition) {
    const sql = `SELECT * FROM ${tableName} WHERE ${condition};`;
    console.log(sql);
    return this.executeSQL(sql);
  }

  delete(tableName,condition){
    const sql = `DELETE FROM ${tableName} WHERE ${condition};`;
    return this.executeSQL(sql);
  }

  async insert (table:string,insert:ColumnsDto){
    let fieldString = "";
    let valueString = "";
    insert.columns.forEach(element => {
      fieldString = (fieldString=='')? element.name : `${fieldString},${element.name}`;
      valueString = (valueString =='')? `${this.escapeSql(element.value)}`: `${valueString},${this.escapeSql(element.value)}`;
    });
    const sql = `INSERT INTO ${table} (${fieldString}) VALUES (${valueString});`;
    console.log(sql);
    try{
      const result = await this.executeSQL(sql);
      return result;
    }catch (error){
      throw new InternalServerErrorException({message:error.sqlMessage,code:error.code});
    }
  }

  async update(table:string,update:ColumnsDto,condition:string){
    let fieldString = "";
    update.columns.forEach(element => {
      fieldString = (fieldString =='') ? `${element.name}=${this.escapeSql(element.value)}`:`${fieldString},${element.name}=${this.escapeSql(element.value)}`;
    });
    const sql = `UPDATE ${table} SET ${fieldString} WHERE ${condition};`;
    console.log(sql);
    try {
      const result  = await this.executeSQL(sql);
      return result;
    }catch (error) {
      throw new InternalServerErrorException({message:error.sqlMessage,code:error.code});
    }
  }

  parseColumnsDto(insertObj):ColumnsDto{
    let columnsDto = new ColumnsDto();
      columnsDto.columns = [];
      Object.entries(insertObj).forEach(
        ([key,value])=>{
          if (typeof value !== 'undefined' && value !== null){
            columnsDto.columns.push(new ColumnPair({name:key,value:value.toString()}))
          }
        }
      );
      return columnsDto;
    
  }

  parseColumnCondition(obj: ColumnsDto): string {
    let fieldString = "";
    obj.columns.forEach(element => {
      fieldString = (fieldString == '') ? `${element.name}=${this.escapeSql(element.value)}` : `${fieldString} and ${element.name}=${this.escapeSql(element.value)}`;
    });
    return fieldString;
  }

  castInArray(arr: Array<string>) {
    let inArr = ''

    arr.forEach(e => {
      // console.log("🚀 ~ file: database.service.ts ~ line 165 ~ DatabaseService ~ castInArray ~ e", e)
      inArr += `${e},`;
    })
    // console.log("🚀 ~ file: database.service.ts ~ line 167 ~ DatabaseService ~ castInArray ~ inArr", inArr)
    inArr = inArr.substring(0, inArr.length - 1)
    return `(${inArr})`;
  }

  getOne(data) {
    return JSON.parse(JSON.stringify(data))[0];
  }

  get(data) {
    return JSON.parse(JSON.stringify(data));
  }

  escapeSql(value: string) {
    return SqlString.escape(value);
  }

    /**
   * Sets sql mode to tradional mode
   */
     async setSqlMode() {

      const sql = `SET SQL_MODE = '${this.sqlMode}'`;
  
      return new Promise((resolve, reject) => {
        const callback = (error, results, fields) => {
          if (error) {
            reject(error)
          }
          else {
            resolve(results)
            // console.log("set sql mode ", sql);
          };
        };
        this.con.query(sql, callback);
      })
    }

    async updateDatabase() {
      await this.createMigrateTable();
      const sqlFolder = './dist/sql/';
      const fs = require('fs');
  
      await fs.readdir(sqlFolder, async (err, files) => {
        for await (const file of files) {
          const sql = `SELECT * FROM db_migration where migrate_file='${file}'`;
          try {
            const dumpExists = await this.executeSQL(sql);
            if (!dumpExists.length) {
              if (file === 'Dump20220127.sql') {
                this.insert(this.migrateTable, new ColumnsDto({
                  columns: [
                    new ColumnPair({ name: "migrate_file", value: file })
                  ]
                }))
                continue;
              }
              const _this = this;
              await fs.readFile(`${sqlFolder}${file}`, 'utf8', async function (err, data) {
                // console.log("🚀 ~ file: database.service.ts ~ line 196 ~ DatabaseService ~ data", data)
                _this.executeSQL(data).then(async result => {
                  // console.log("🚀 ~ file: database.service.ts ~ line 199 ~ DatabaseService ~ _this.executeSQL ~ result", result)
                  _this.insert(_this.migrateTable, new ColumnsDto({
                    columns: [
                      new ColumnPair({ name: "migrate_file", value: file })
                    ]
                  }))
                }).catch(err => {
                  // console.log("🚀 ~ file: database.service.ts ~ line 202 ~ DatabaseService ~ _this.executeSQL ~ err", err)
                  if (err.code === 'ER_UNKNOWN_ERROR' && err.sqlMessage === 'client has multi-statement capability disabled') {
                    _this.insert(_this.migrateTable, new ColumnsDto({
                      columns: [
                        new ColumnPair({ name: "migrate_file", value: file })
                      ]
                    }))
                  }
                });
              });
            }
          } catch (error) {
            // console.log("🚀 ~ file: database.service.ts ~ line 221 ~ DatabaseService ~ forawait ~ error", error)
          }
        }
      });
    }

    private createMigrateTable() {
      return this.executeSQL(`select * from ${this.migrateTable}`).then(result => {
      }).catch(err => {
        console.log(err.code);
        if (err.code === "ER_NO_SUCH_TABLE") {
          const createTableSql = `
          CREATE TABLE ${this.migrateTable} (
            id int(11) NOT NULL AUTO_INCREMENT,
            migrate_file text null,
            created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
          )`
          this.executeSQL(createTableSql).then(result => {
            // console.log("🚀 ~ file: database.service.ts ~ line 238 ~ DatabaseService ~ this.executeSQL ~ result", result)
  
          }).catch(err => {
            // console.log("🚀 ~ file: database.service.ts ~ line 241 ~ DatabaseService ~ this.executeSQL ~ err", err)
          })
        }
      })
    }
}
