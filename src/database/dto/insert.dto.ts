import { ApiProperty } from "@nestjs/swagger";

export class ColumnPair {
    constructor(partial: Partial<ColumnPair>) {
      Object.assign(this, partial);
    }
  
    @ApiProperty()
    name: string;
  
    @ApiProperty()
    value: string;
  }
  
  export class ColumnsDto {
  
    constructor(partial?: Partial<ColumnsDto>) {
      Object.assign(this, partial);
    }
  
    @ApiProperty({ type: ColumnPair, isArray: true })
    columns: ColumnPair[];
  
  }