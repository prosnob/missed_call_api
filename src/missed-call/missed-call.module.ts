import { Module } from '@nestjs/common';
import { MissedCallService } from './missed-call.service';
import { MissedCallController } from './missed-call.controller';
import { DatabaseService } from 'src/database/database.service';

@Module({
  controllers: [MissedCallController],
  providers: [MissedCallService,DatabaseService]
})
export class MissedCallModule {}
