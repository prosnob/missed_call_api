import { ApiProperty } from "@nestjs/swagger";

export class CustomSQLDto {

      @ApiProperty({example: "statment"})
      sql: string;
    
    }