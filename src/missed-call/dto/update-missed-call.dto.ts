import { PartialType } from '@nestjs/swagger';
import { CreateMissedCallDto } from './create-missed-call.dto';

export class UpdateMissedCallDto extends PartialType(CreateMissedCallDto) {}
