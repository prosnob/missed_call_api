import { Test, TestingModule } from '@nestjs/testing';
import { MissedCallService } from './missed-call.service';

describe('MissedCallService', () => {
  let service: MissedCallService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MissedCallService],
    }).compile();

    service = module.get<MissedCallService>(MissedCallService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
