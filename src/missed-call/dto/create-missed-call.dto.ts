import { ApiProperty } from "@nestjs/swagger";

export class CreateMissedCallDto{
    constructor(init?:Partial<CreateMissedCallDto>){
        Object.assign(this,init);
    }
    @ApiProperty()
    phone:string;
    @ApiProperty()
    missed_timestamp:number;
    @ApiProperty()
    status:string;
    @ApiProperty()
    time:Date;
}
