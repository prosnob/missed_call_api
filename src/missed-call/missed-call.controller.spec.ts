import { Test, TestingModule } from '@nestjs/testing';
import { MissedCallController } from './missed-call.controller';
import { MissedCallService } from './missed-call.service';

describe('MissedCallController', () => {
  let controller: MissedCallController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MissedCallController],
      providers: [MissedCallService],
    }).compile();

    controller = module.get<MissedCallController>(MissedCallController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
